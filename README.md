#Space Mission
Test-zadatak potencijalong poslodavca  

##2 Uvod
U sklopu konzorcija za opremanje ESA svemirske misije treba isporučiti prototip aplikacije za
praćenje prehrane astronauta. Aplikacija će u konačnici biti izrađena za specijalizirani hardware koji se
koristi u misijama, a prototip se izvodi kao web aplikacija.

Astronauti kroz aplikaciju evidentiraju dnevni unos namirnica, a sama aplikacija im sugerira izbor
namirnica obzirom na vitaminski plan za taj dan.

## 3 Funkcionalni zahtjevi

###3.1 Popis namirnica
Popis namirnica koje su na raspolaganju za vrijeme trajanja misije se dobija putem servisa [...].
Popis je potrebno vizualizirati u obliku stable čije listove je moguće odvući na tablicu za evidenciju
obroka. Namirnice su u stablu organizirane u više razina.

###3.2 Popis vitamina
Popis vitamina za trenutni dan se dobija sa servisa [...].
dayOfTheWeek je redni broj dana u tjednu (1-7, tjedan počinje nedjeljom).
Potrebno je prikazati listu svih vitamina (A, B1, C, D, E), a posebno označiti one koji su u planu za
trenutni dan.

###3.3 Evidencija obroka
Evidencija dnevnog unosa namirnica se vodi kroz dinamički generiranu tablicu. Prvi redak tablice sadrži
naziv namirnice, a ostali reci služe za evidenciju pojedinih obroka. U svakoj ćeliji se evidentira broj
namirnica konzumiranih u obroku.

Pri pokretanju aplikacije je prostor za tablicu prazan i sadrži samo placeholder na koji se može dovući
element iz stabla sa popisom namirnica (drag’n’drop).

Kad se dovuče prva namirnica kreirat će se tablica sa jednom kolonom i dva retka (u prvom je naziv
namirnice, a drugi je prazan za unos brojke). Ako se (bilo gdje) na tablicu dovuče još jedna namirnica iz
stable, na desnoj strani se automatski dodaje nova kolona (sa onoliko redaka koliko trenutno sadrži
tablica).

Na dnu tablice se uvijek nalazi jedan prazan redak u koji se unose podaci o novom obroku. Korisnik ga ne
treba dodavati ručno nego se automatski generira po unosu vrijednosti u bilo koju ćeliju zadnjeg (do
tada praznog) retka.

Korisnik treba moći obrisati bilo koji redak iz tablice (osim zaglavnog s nazivima namirnica).

###3.4 UI Mock
![UI Mock](gui-mock.png)

