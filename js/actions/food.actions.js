const _ = require('lodash');
const Request = require('superagent');

const AppDispatcher = require('../dispatcher/AppDispatcher');
const Events = require('../events/food.events');
const Path = require('../constants/path.const');

const storage = require('../util/simple.storage');

const FoodActions = {

    fetchDailyVitamins(day) {
        Request
            .get(`${Path.DAILY_VITAMIN_LIST}${day}`)
            .end((err, res) => {
                try {
                    const payload = JSON.parse(res.text);

                    if (payload.status !== 0) {
                        AppDispatcher.dispatch({
                            actionType: Events.VITAMINS_LOAD_ERROR,
                            type: 'vitamins',
                            message: payload.message
                        });
                    }
                    else {
                        AppDispatcher.dispatch({
                            actionType: Events.VITAMINS_LOADED,
                            data: payload.data
                        });
                    }
                }
                catch(e) {
                    AppDispatcher.dispatch({
                        actionType: Events.VITAMINS_LOAD_ERROR,
                        type: 'vitamins',
                        message: err.message
                    });
                }
            });
    },

    fetchFood() {
        Request
            .get(Path.AVAILABLE_FOOD_LIST)
            .end((err, res) => {
                try {
                    const payload = JSON.parse(res.text);

                    if (payload.status !== 0) {
                        AppDispatcher.dispatch({
                            actionType: Events.FOOD_LOAD_ERROR,
                            type: 'food',
                            message: payload.message
                        });
                    }
                    else {
                        AppDispatcher.dispatch({
                            actionType: Events.FOOD_LOADED,
                            data: payload.data
                        });
                    }
                }
                catch(e) {
                    AppDispatcher.dispatch({
                        actionType: Events.FOOD_LOAD_ERROR,
                        type: 'food',
                        message: err.message
                    });
                }
            });
    },

    fetchIntake() {
        if (storage('intake')) {
            AppDispatcher.dispatch({
                actionType: Events.LOG_CACHE_LOADED,
                intake: storage('intake')
            });
        }
    },

    addLogColumn(foodId) {
        AppDispatcher.dispatch({
            actionType: Events.LOG_COLUMN_ADDED,
            foodId
        });
    },

    deleteLogRow(index) {
        AppDispatcher.dispatch({
            actionType: Events.LOG_ROW_DELETED,
            index
        });
    },

    addLogEntry(row, cell, value) {
        AppDispatcher.dispatch({
            actionType: Events.LOG_ENTRY_CHANGE,
            row,
            cell,
            value
        });
    },

    clearMessage() {
        AppDispatcher.dispatch({
            actionType: Events.CLEAR_MESSAGE
        });
    },

    saveIntake(intakeLog) {
        if (intakeLog.length < 3) {
            AppDispatcher.dispatch({
                actionType: Events.LOG_SAVE_ERROR,
                type: 'save',
                message: 'Nothing to save.\nWhy waist bandwidth, government resources and taxpayers\' money on sending empty logs?\nTake a space-walk and try again'
            });
            return;
        }

        Request
            .post(Path.RECORD_INTAKE_LOG)
            .send({
                date: new Date().toISOString().substr(0, 10),
                dailyIntake: JSON.stringify(_.dropRight(intakeLog))
            })
            .end((err, res) => {
                const response = JSON.parse(_.get(res, 'text', '{}'));
                if (!_.isNull(err) || response.status !== 0) {
                    const connectionError = _.get(err, 'message', '');
                    const responseError = _.get(response, 'message', '');
                    AppDispatcher.dispatch({
                        actionType: Events.LOG_SAVE_ERROR,
                        type: 'save',
                        message: connectionError + responseError
                    });
                }
                else {
                    AppDispatcher.dispatch({
                        actionType: Events.LOG_SAVE_SUCCESS
                    });
                }
            });
    },

    resetIntake() {
        this.cacheIntake(null);
        AppDispatcher.dispatch({
            actionType: Events.LOG_CACHE_LOADED,
            intake: [[],[]]
        });
    },

    cacheIntake(intake) {
        storage('intake', intake);
    }
};

module.exports = FoodActions;