const keyMirror = require('keymirror');

module.exports = keyMirror({
    VITAMINS_LOADED: null,
    VITAMINS_LOAD_ERROR: null,
    FOOD_LOADED: null,
    FOOD_LOAD_ERROR: null,
    LOG_CACHE_LOADED: null,

    LOG_COLUMN_ADDED: null,
    LOG_ROW_DELETED: null,
    LOG_ENTRY_CHANGE: null,

    LOG_SAVE_SUCCESS: null,
    LOG_SAVE_ERROR: null,

    CLEAR_MESSAGE: null
});