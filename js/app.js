const React = require('react');
const ReactDOM = require('react-dom');

const App = require('./components/space-mission.react');

ReactDOM.render(
    <App/>,
    document.getElementById('app')
);
