module.exports = {
    DAILY_VITAMIN_LIST: 'https://private-2686f-spacemission.apiary-mock.com/dailyvitamins/',
    AVAILABLE_FOOD_LIST: 'https://private-2686f-spacemission.apiary-mock.com/food',
    RECORD_INTAKE_LOG: 'https://private-2686f-spacemission.apiary-mock.com/dailyintake'
};