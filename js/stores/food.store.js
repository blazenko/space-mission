const React = require('react');
const update = require('react-addons-update');
const _ = require('lodash');
const EventEmitter = require('events').EventEmitter;

const AppDispatcher = require('../dispatcher/AppDispatcher');
const Events = require('../events/food.events');

const CHANGE_EVENT = 'change';

let _vitamins = [],
    _food = [],
    _intake = [[],[]],
    _sysMsg = '';

const _foodList = [];

const FoodStore = _.assign({}, EventEmitter.prototype, {

    /* GET / PUBLIC */
    getVitamins() {
        return _vitamins;
    },

    getFood() {
        return _food;
    },

    getIntake() {
        if (_.isEmpty(_food)) {
            return [[],[]];
        }
        return _.cloneDeep(_intake);
    },

    getFoodItem(foodId) {
        return _.find(_foodList, {code: foodId});
    },

    getFeedback() {
        return _sysMsg;
    },


    /* SET / PRIVATE */
    _setVitamins(data) {
        _vitamins = _.map(data, v => v.code);
    },

    _setFeedback(isError, type, message) {
        if (isError) {
            switch (type) {
                case 'vitamins':
                    _sysMsg = `Error getting daily vitamins: ${message}.\nThis is your chance to avoid the terror of healthy food.\nPress Ctrl+Alt+Del for the secret junk food menu.`;
                    break;
                case 'save':
                    _sysMsg = `Error saving data: ${message}.`;
                    break;
                default:
                    _sysMsg = `Error: ${message}`;
            }
        }
        else {
            switch (type) {
                case 'save':
                    _sysMsg = 'Succesfully saved data.';
                    break;
                default:
                    _sysMsg = '';
            }
        }
    },

    _clearFeedback() {
        _sysMsg = '';
    },

    _setFood(data) {
        _food = data;
    },

    _makeFoodList(data) {
        this._addToFoodList(data);
    },

    _addToFoodList(arr) {
        _.forEach(arr, item => {
            if (!_.isEmpty(item.children)) {
                this._addToFoodList(item.children);
            }
            else {
                _foodList.push(item);
            }
        });
    },

    _setIntake(intake) {
        _intake = intake;
    },

    _addLogColumn(foodId) {
        _intake = _.map(_intake, (row, i) => {
            if (i === 0) {
                return [...row, foodId];
            }

            return [...row, 0];
        });
    },

    _deleteLogRow(index) {
        _intake = _.filter(_intake, (row, i) => i != index);
    },

    _addLogEntry({row, cell, value}) {
        if (!_intake[row+1] &&  !isNaN(value) && value !== 0) {
            const emptyRow = [];
            _.times(_intake[0].length, () => emptyRow.push(0));
            _intake = update(_intake, {$push: [emptyRow]});
        }

        const data = isNaN(value) ? 0 : value;
        _intake = _.map(_intake, (_row, i) => {
            if (i === row) _row.splice(cell, 1, data);
            return [..._row];
        });
    },



    /* STORE API */
    _emitChange() {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

FoodStore.dispatchToken = AppDispatcher.register((payload) => {
    switch (payload.actionType) {
        case Events.VITAMINS_LOADED:
            FoodStore._setVitamins(payload.data);
            FoodStore._setFeedback(false, 'vitamins');
            FoodStore._emitChange();
            break;

        case Events.FOOD_LOADED:
            FoodStore._setFood(payload.data);
            FoodStore._makeFoodList(payload.data);
            FoodStore._emitChange();
            break;

        case Events.LOG_CACHE_LOADED:
            FoodStore._setIntake(payload.intake);
            FoodStore._emitChange();
            break;

        case Events.VITAMINS_LOAD_ERROR:
            FoodStore._setVitamins([]);
            FoodStore._setFeedback(true, payload.type, payload.message);
            FoodStore._emitChange();
            break;

        case Events.FOOD_LOAD_ERROR:
        case Events.LOG_SAVE_ERROR:
            FoodStore._setFeedback(true, payload.type, payload.message);
            FoodStore._emitChange();
            break;

        case Events.LOG_COLUMN_ADDED:
            FoodStore._addLogColumn(payload.foodId);
            FoodStore._emitChange();
            break;

        case Events.LOG_ROW_DELETED:
            FoodStore._deleteLogRow(payload.index);
            FoodStore._emitChange();
            break;

        case Events.LOG_ENTRY_CHANGE:
            FoodStore._addLogEntry(payload);
            FoodStore._emitChange();
            break;

        case Events.LOG_SAVE_SUCCESS:
            FoodStore._setFeedback(false, 'save');
            FoodStore._emitChange();
            break;

        case Events.CLEAR_MESSAGE:
            FoodStore._clearFeedback();
            FoodStore._emitChange();
            break;
    }
});

module.exports = FoodStore;


