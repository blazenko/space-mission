'use strict';

const React = require('react');
const PureRenderMixin = require('react-addons-pure-render-mixin');
const ClassNames = require('classnames');

const Vitamin = React.createClass({
    mixins: [PureRenderMixin],

    propTypes: {
        name: React.PropTypes.string,
        code: React.PropTypes.number,
        active: React.PropTypes.bool
    },


    render() {
        const className = ClassNames({
            vitamin: true,
            active: this.props.active
        });

        return (
            <div className={className}>{ this.props.name }</div>
        );
    }
});

module.exports = Vitamin;
