'use strict';

const React = require('react');
const _ = require('lodash');

const Vitamin = require('./_vitamin.react');

const Vitamins = React.createClass({

    propTypes: {
        allVitamins: React.PropTypes.object,
        dailyVitamins: React.PropTypes.array
    },

    shouldComponentUpdate(nextProps) {
        return !_.isEqual(this.props, nextProps);
    },

    render() {
        return (
            <div className="vitamins">
                { this._renderVitamins() }
            </div>
        );
    },

    _renderVitamins() {
        return _.map(this.props.allVitamins, (val, key) => {
            const active = _.includes(this.props.dailyVitamins, val);
            return <Vitamin key={key} name={key} active={active}/>;
        });
    }
});

module.exports = Vitamins;
