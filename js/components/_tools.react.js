'use strict';

const React = require('react');
const _ = require('lodash');

const DemoTools = React.createClass({

    propTypes: {
        day: React.PropTypes.number,
        onChange: React.PropTypes.func,
        onReset: React.PropTypes.func
    },


    render() {
        return (
            <div className="demo-tools">
                Change day
                <br/>
                <span className="tool-arrow glyphicon glyphicon-triangle-left" onClick={this._prev}/>
                {this.props.day}
                <span className="tool-arrow glyphicon glyphicon-triangle-right" onClick={this._next}/>
                <br/>
                <button className="btn btn-reset btn-xs" onClick={this.props.onReset}>Reset intake</button>
            </div>
        );
    },

    _next() {
        const day = this.props.day === 7 ? 1 : this.props.day + 1;
        this.props.onChange(day);
    },

    _prev() {
        const day = this.props.day === 1 ? 7 : this.props.day - 1;
        this.props.onChange(day);
    }
});

module.exports = DemoTools;
