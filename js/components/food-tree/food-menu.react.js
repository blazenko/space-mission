'use strict';

const React = require('react');
const _ = require('lodash');

const Category = require('./_food-category.react');

const FoodTree = React.createClass({

    propTypes: {
        food: React.PropTypes.array,
        dailyVitamins: React.PropTypes.array
    },

    shouldComponentUpdate(nextProps) {
        return !_.isEqual(this.props, nextProps);
    },

    render() {
        return (
            <div className="food-menu">
                { this._renderFoodTree() }
            </div>
        );
    },

    _renderFoodTree() {
        return _.map(this.props.food, (cat, i) => {
            return (
                <Category
                    name={cat.name}
                    items={cat.children}
                    dailyVitamins={this.props.dailyVitamins}
                    key={i}
                    index={i} />
            );
        });
    }
});

module.exports = FoodTree;
