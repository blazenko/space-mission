'use strict';

const React = require('react');
const _ = require('lodash');
const PureRenderMixin = require('react-addons-pure-render-mixin');

const ClassNames = require('classnames');
const { Draggable } = require('react-drag-and-drop');

const FoodItem = React.createClass({
    mixins: [PureRenderMixin],

    propTypes: {
        name: React.PropTypes.string,
        code: React.PropTypes.number,
        active: React.PropTypes.bool
    },

    render() {
        return (
            <Draggable type="food" data={this.props.code} enabled={true}>
                <div className={ClassNames('food-item', {active: this.props.active})}>
                    <span>{ this.props.name }</span>
                </div>
            </Draggable>
        );
    }
});

module.exports = FoodItem;
