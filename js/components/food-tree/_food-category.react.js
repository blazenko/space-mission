'use strict';

const React = require('react');
const CollapsibleList = require('react-addons-css-transition-group');

const ClassNames = require('classnames');
const _ = require('lodash');

const Item = require('./_food-item.react');

const FoodCategory = React.createClass({

    propTypes: {
        name: React.PropTypes.string,
        items: React.PropTypes.array,
        dailyVitamins: React.PropTypes.array,
        index: React.PropTypes.number
    },

    getInitialState() {
        return {
            collapsed: this.props.index > 0
        };
    },

    shouldComponentUpdate(nextProps, nextState) {
        return !_.isEqual(this.props, nextProps) || !_.isEqual(this.state, nextState);
    },

    render() {
        const folderClass = ClassNames({
            'folder glyphicon': true,
            'glyphicon-folder-open': !this.state.collapsed,
            'glyphicon-folder-close': this.state.collapsed
        });

        return (
            <div className="food-item category">
                <span className={folderClass}/>
                <span className="category-name" onClick={this._toggleCollapsed}>
                    { this.props.name }
                </span>
                <CollapsibleList
                    component="div"
                    transitionName="tree"
                    transitionAppear={true}
                    transitionAppearTimeout={500}
                    transitionEnterTimeout={500}
                    transitionLeaveTimeout={500} >

                    { this._renderItems() }

                </CollapsibleList>
            </div>
        );
    },

    _renderItems() {
        if (this.state.collapsed) {
            return null;
        }

        return _.map(this.props.items, (item, i) => {
            const active = !_.isEmpty(_.intersection(this.props.dailyVitamins, _.get(item, 'vitamins', [])));
            return _.isUndefined(item.children)
                ? <Item {...item} key={i} active={active} />
                : <FoodCategory name={item.name} items={item.children} key={i} dailyVitamins={this.props.dailyVitamins} />;
        });
    },

    _toggleCollapsed() {
        this.setState({
            collapsed: !this.state.collapsed
        });

    }
});

module.exports = FoodCategory;
