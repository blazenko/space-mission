'use strict';

const React = require('react');
const _ = require('lodash');
const PureRenderMixin = require('react-addons-pure-render-mixin');

const Store = require('../../stores/food.store');

const HeaderCell = React.createClass({
    mixins: [PureRenderMixin],

    propTypes: {
        foodId: React.PropTypes.number,
        width: React.PropTypes.number
    },

    render() {
        const food = Store.getFoodItem(this.props.foodId);

        return (
            <div className="cell cell-head" style={{width: `${this.props.width}%`}}>
                { food.name }
            </div>
        );
    }
});

module.exports = HeaderCell;
