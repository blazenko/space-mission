'use strict';

const React = require('react');
const _ = require('lodash');

const Cell = require('./_cell.react');
const RowDelete = require('./_row-delete.react');

const IntakeRow = React.createClass({

    propTypes: {
        data: React.PropTypes.array,
        columnWidth: React.PropTypes.number,
        row: React.PropTypes.number,
        onDelete: React.PropTypes.func
    },

    shouldComponentUpdate(nextProps) {
        return !_.isEqual(this.props, nextProps);
    },

    render() {
        return (
            <div className="row intake-row">
                <RowDelete row={this.props.row} onDelete={this.props.onDelete} />
                { this._renderCells() }
            </div>
        );
    },

    _renderCells() {
        return _.map(this.props.data, (value, i) => {
            return <Cell value={value} width={this.props.columnWidth} key={i} row={this.props.row} cell={i} />;
        });
    }
});

module.exports = IntakeRow;
