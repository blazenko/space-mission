'use strict';

const React = require('react');
const _ = require('lodash');
const PureRenderMixin = require('react-addons-pure-render-mixin');

const Actions = require('../../actions/food.actions');

const DataCell = React.createClass({
    mixins: [PureRenderMixin],

    propTypes: {
        width: React.PropTypes.number,
        row: React.PropTypes.number,
        cell: React.PropTypes.number,
        value: React.PropTypes.number
    },

    render() {
        return (
            <div className="cell" style={{width: `${this.props.width}%`}}>
                <input value={this.props.value || ''} onChange={this._onDataChange} />
            </div>
        );
    },

    _onDataChange(e) {
        Actions.addLogEntry(this.props.row, this.props.cell, parseInt(e.target.value));
    }
});

module.exports = DataCell;
