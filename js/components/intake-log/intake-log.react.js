'use strict';

const React = require('react');
const { Droppable } = require('react-drag-and-drop');
const Animate = require('react-addons-css-transition-group');
const _ = require('lodash');

const HeaderRow = require('./_row-head.react');
const Row = require('./_row.react');

const IntakeLog = React.createClass({

    propTypes: {
        intake: React.PropTypes.array,
        onAddFood: React.PropTypes.func,
        onDeleteMeal: React.PropTypes.func
    },

    shouldComponentUpdate(nextProps) {
        return !_.isEqual(this.props, nextProps);
    },

    render() {
        return (
            <Droppable
                types={['food']}
                id="intake-log"
                className="container-fluid"
                style={{width: this._calcContainerWidth() }}
                onDrop={this._onDrop}>

                <Animate
                    component="div"
                    transitionName="row"
                    transitionAppearTimeout={200}
                    transitionEnterTimeout={200}
                    transitionLeaveTimeout={500} >

                    { this._renderRows() }

                </Animate>
            </Droppable>
        );
    },

    _renderRows() {
        return _.map(this.props.intake, (row, i) => {
            if (i == 0) return <HeaderRow
                                    foods={row}
                                    key={i}
                                    columnWidth={this._calcColumnWidth()} />;

            return <Row data={row}
                        key={i}
                        row={i}
                        columnWidth={this._calcColumnWidth()}
                        onDelete={this.props.onDeleteMeal} />;
        });
    },

    _calcColumnWidth() {
        const numCols = this.props.intake[0].length;
        if (numCols <= 4) return 20;
        return 100 / numCols;
    },

    _calcContainerWidth() {
        const numCols = this.props.intake[0].length;
        if (numCols <= 7) return '100%';
        return (180 * numCols) + 'px';
    },

    _onDrop(data, e) {
        const foodId = parseInt(data.food);

        if (_.includes(this.props.intake[0], foodId)) return;

        this.props.onAddFood(foodId);
    }
});

module.exports = IntakeLog;
