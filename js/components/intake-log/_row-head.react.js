'use strict';

const React = require('react');
const _ = require('lodash');

const HeaderCell = require('./_cell-head.react');

const HeaderRow = React.createClass({

    propTypes: {
        foods: React.PropTypes.array,
        columnWidth: React.PropTypes.number
    },

    shouldComponentUpdate(nextProps) {
        return !_.isEqual(this.props, nextProps);
    },

    render() {
        return (
            <div className="row intake-row">
                { this._renderCells() }
            </div>
        );
    },

    _renderCells() {
        return _.map(this.props.foods, (foodId, i) => {
            return <HeaderCell foodId={foodId} width={this.props.columnWidth} key={i} />;
        });
    }
});

module.exports = HeaderRow;
