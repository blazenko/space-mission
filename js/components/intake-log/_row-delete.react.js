'use strict';

const React = require('react');
const PureRenderMixin = require('react-addons-pure-render-mixin');

const RowDelete = React.createClass({
    mixins: [PureRenderMixin],

    propTypes: {
        row: React.PropTypes.number,
        onDelete: React.PropTypes.func
    },

    getInitialState() {
        return {
            confirm: false
        };
    },

    render() {
        return (
            <div className="row-delete">
                { this.state.confirm
                    ? <Confirm onConfirm={this._confirmDelete} onCancel={this._toggle} />
                    : <Trash onClick={this._toggle} /> }
            </div>
        );
    },

    _toggle() {
        this.setState({
            confirm: !this.state.confirm
        });
    },

    _confirmDelete() {
        this.props.onDelete(this.props.row);
        this._toggle();
    }
});

const Trash = (props) => {
    return (
        <div className="btn-container">
            <span className="glyphicon glyphicon-trash" onClick={props.onClick}/>
        </div>
    );
};
const Confirm = (props) => {
    return (
        <div className="btn-container">
            <span className="glyphicon glyphicon-ok" onClick={props.onConfirm}/>
            <span className="glyphicon glyphicon-remove" onClick={props.onCancel} />
        </div>
    );
};


module.exports = RowDelete;
