'use strict';

const React = require('react');
const _ = require('lodash');
const ClassNames = require('classnames');

const Feedback = React.createClass({

    propTypes: {
        msg: React.PropTypes.string,
        onClose: React.PropTypes.func
    },

    render() {
        if (_.isEmpty(this.props.msg)) {
            return null;
        }

        const className = ClassNames({
            'sys-msg': true,
            'error': _.startsWith(this.props.msg, 'Error'),
            'success': !_.startsWith(this.props.msg, 'Error')
        });

        return (
            <div className={className} onClick={this.props.onClose}>
                { this.props.msg }
                <span className="msg-close glyphicon glyphicon-remove" />
            </div>
        );
    }
});

module.exports = Feedback;
