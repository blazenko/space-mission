'use strict';

const React = require('react');
const _ = require('lodash');

const Actions = require('../actions/food.actions.js');
const Store = require('../stores/food.store.js');

const VitaminsList = require('../constants/vitamins.const');

const Vitamins = require('./vitamins/vitamins.react');
const FoodMenu = require('./food-tree/food-menu.react');
const IntakeLog = require('./intake-log/intake-log.react');
const Feedback = require('./_feedback.react');
const DemoTools = require('./_tools.react');

const App = React.createClass({

    getInitialState() {
        return {
            day: new Date().getDay() + 1,
            food: [],
            intake: [[],[]],
            vitamins: [],
            msg: ''
        };
    },

    componentDidMount() {
        Store.addChangeListener(this._onStoreChange);
        Actions.fetchDailyVitamins(this.state.day);
        Actions.fetchFood();
        Actions.fetchIntake();
    },


    componentDidUpdate(prevProps, prevState) {
        Actions.cacheIntake(this.state.intake);
    },

    componentWillUnmount() {
        Store.removeChangeListener(this._onStoreChange);
    },


    render() {
        return (
            <div className='container-fluid'>
                <div className="row base-row base-1">
                    <div className="col-md-2">
                        <Vitamins
                            allVitamins={VitaminsList}
                            dailyVitamins={this.state.vitamins} />
                    </div>
                    <div className="col-md-8">
                        <Feedback msg={this.state.msg} onClose={this._clearMessage} />
                    </div>
                    <div className="col-md-2">
                        <DemoTools day={this.state.day} onChange={this._setDay} onReset={this._resetIntake} />
                    </div>
                </div>
                <div className="row base-row base-2">
                    <div className="col-md-2 food-col">
                        <FoodMenu food={this.state.food} dailyVitamins={this.state.vitamins} />
                    </div>

                    <div className="col-md-10">
                        <div className="viewport">
                            <IntakeLog
                                intake={this.state.intake}
                                onAddFood={Actions.addLogColumn}
                                onDeleteMeal={Actions.deleteLogRow}/>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <button className="btn btn-primary btn-save pull-right" onClick={this._saveIntake}>SAVE DATA</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    },

    _saveIntake() {
        Actions.saveIntake(this.state.intake);
    },

    _resetIntake() {
        Actions.resetIntake();
    },

    _setDay(day) {
        this.setState({
            day
        });
        Actions.fetchDailyVitamins(day);
    },

    _clearMessage() {
        Actions.clearMessage();
    },

    _onStoreChange() {
        this.setState({
            vitamins: Store.getVitamins(),
            food: Store.getFood(),
            intake: Store.getIntake(),
            msg: Store.getFeedback()
        });
    }
});

module.exports = App;